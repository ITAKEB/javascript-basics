/**
 *
 * @returns {Null}
 */
module.exports.Logger = function Logger() {
    // Your implementation here
    let logs = []
    this.log = function (log) {
        logs.push(log)
    }
    this.getLog = () => { return logs }

    this.clearLog = function () {
        logs = []
    }
};

/**
 *
 * @returns {Array}
 */
// Change this function and implement task
Array.prototype.shuffle = function () {
    let aux = [...this]
    for (let i = 0; i < this.length; i++) {
        aux.push(this[i])
    }
    for (let i = this.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        let temp = this[i];
        this[i] = this[j];
        this[j] = temp;
    }
    if (aux == this) {
        return this.shuffle()
    } else {
        return this
    }
}