/**
 *
 * @param arr: {Array}
 * @returns {Array}
 */
module.exports.numberOfDuplicates = function numberOfDuplicates(arr) {
    // Your implementation here
    let aux = arr.map((e, i) => { return { index: i, value: e, count: 0 } }
    )
    aux = aux.sort((a, b) => {
        if (a.value > b.value) {
            return 1
        } else if (a.value < b.value) {
            return -1
        } else {
            return 0
        }
    })

    let cont = 0
    for (let i = 0; i < aux.length - 1; i++) {
        if (aux[i].value == aux[i + 1].value) {
            cont++;
            aux[i].count = cont
            continue
        }
        cont++;
        aux[i].count = cont
        cont = 0
    }
    aux[aux.length - 1].count = arr.filter((b) => aux[aux.length - 1].value === b).length

    aux = aux.sort((a, b) => {
        if (a.index > b.index) {
            return 1
        } else if (a.index < b.index) {
            return -1
        } else {
            return 0
        }
    })

    const count = aux.map((e) => e.count)

    return count
}


/**
 *
 * @param obj: {Object}
 * @returns {Number}
 */
module.exports.countObjectStrength = function countObjectStrength(obj) {
    // Your implementation here
    const count = Object.values(obj).map((element) => {
        switch (typeof element) {
            case "undefined":
                return 0
            case "boolean":
                return 1
            case "number":
                return 2
            case "string":
                return 3
            case "object":
                return 5
            case "function":
                return 7
            case "bigint":
                return 9
            case "symbol":
                return 10
            default:
                return -1
        }
    })
    let sum = 0
    count.forEach((element) => sum += element)
    return sum
};