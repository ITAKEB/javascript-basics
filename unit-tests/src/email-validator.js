const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'mail.com', 'icloud.com']

export function validate(email) {
    const reg = /^[a-zA-Z]{3,}\@[a-zA-Z]+\.[a-zA-Z]+$/

    if (!reg.test(email)) {
        return false
    }

    let endingValidation = false
    const emailEnding = email.split('@')[1]
    VALID_EMAIL_ENDINGS.forEach((valid) => {
        if (emailEnding === (valid)) {
            endingValidation = true
        }
    })

    return endingValidation
}

export function validateWithThrow(email) {
    const reg = /^[a-zA-Z]{3,}\@[a-zA-Z]+\.[a-zA-Z]+$/

    if (!reg.test(email)) {
        throw new Error("the provided email is invalid")
    }

    let endingValidation = false
    const emailEnding = email.split('@')[1]
    VALID_EMAIL_ENDINGS.forEach((valid) => {
        if (emailEnding === (valid)) {
            endingValidation = true
        }
    })

    if(endingValidation) {
        return true
    }

    throw new Error("the provided email is invalid")
}

export function validateWithLog(email) {
    const reg = /^[a-zA-Z]{3,}\@[a-zA-Z]+\.[a-zA-Z]+$/

    if (!reg.test(email)) {
        console.log(false)
        return false
    }

    let endingValidation = false
    const emailEnding = email.split('@')[1]
    VALID_EMAIL_ENDINGS.forEach((valid) => {
        if (emailEnding === (valid)) {
            endingValidation = true
        }
    })

    console.log(endingValidation)
    return endingValidation
}

export async function validateAsync(email) {
    const reg = /^[a-zA-Z]{3,}\@[a-zA-Z]+\.[a-zA-Z]+$/

    if (!reg.test(email)) {
        return new Promise((resolve, rejected) => {
            setTimeout(() => {
                rejected("Invalid email")
            }, 10)// simulate an asynchronous operation that takes less than 1 second    
        })
    }

    let endingValidation = false
    const emailEnding = email.split('@')[1]
    VALID_EMAIL_ENDINGS.forEach((valid) => {
        if (emailEnding === (valid)) {
            endingValidation = true
        }
    })

    return new Promise((resolve, rejected) => {
        if(endingValidation){
            setTimeout(() => {
                resolve(endingValidation)
            }, 10) // simulate an asynchronous operation that takes less than 1 second    
        }else{
            rejected("Invalid email")
        }
    })
}
