import { expect } from 'chai'
import sinon from 'sinon'

const { validate, validateAsync, validateWithThrow, validateWithLog } = require("../src/email-validator")

const assert = require("assert")

describe("validate email function - tests", () => {
    it("should return true for given input `example@gmail.com`", () => {
        const email = "example@gmail.com"
        const result = validate(email)
        assert.equal(true, result)
    })

    it("should return true for given input `example@gmail.com`", () => {
        const email = "example@outlook.com"
        const result = validate(email)
        assert.equal(true, result)
    })

    it("should return true for given input `example@gmail.com`", () => {
        const email = "example@mail.com"
        const result = validate(email)
        assert.equal(true, result)
    })

    it("should return true for given input `example@gmail.com`", () => {
        const email = "example@icloud.com"
        const result = validate(email)
        assert.equal(true, result)
    })

    it("should return false for given input `@gmail.com`", () => {
        const email = "@gmail.com"
        const result = validate(email)
        assert.equal(false, result)
    })

    it("should return false for given input `m@gmail.com`", () => {
        const email = "m@gmail.com"
        const result = validate(email)
        assert.equal(false, result)
    })

    it("should return false for given input `example@email.com`", () => {
        const email = "example@email.com"
        const result = validate(email)
        assert.equal(false, result)
    })
})

describe("validate email with throw function - tests", () => {
    it("should return true for given input `example@gmail.com`", async () => {
        const email = "example@gmail.com"
        const result = validateWithThrow(email)
        assert.equal(true, result)
    })

    it("should return error for given input `@gmail.com`", async () => {
        const email = "@gmail.com"
        assert.throws(
            () => {validateWithThrow(email)},
            Error,
            "the provided email is invalid"
        )
    })

    it("should return error for given input `m@gmail.com`", async () => {
        const email = "m@gmail.com"
        assert.throws(
            () => { validateWithThrow(email) },
            Error,
            "the provided email is invalid"
        )
    })

    it("should return error for given input `example@email.com`", async () => {
        const email = "example@email.com"
        assert.throws(
            () => { validateWithThrow(email) },
            Error,
            "the provided email is invalid"
        )
    })
})

describe("validate email with logs function - tests", () => {
    it("should return true for given input `example@gmail.com`", () => {
        const email = "example@gmail.com"
        let consoleSpy = sinon.spy(console, 'log')
        validateWithLog(email)

        assert(consoleSpy.calledWith(true))
        consoleSpy.restore()
    })

    it("should return false for given input `@gmail.com`", () => {
        const email = "@gmail.com"
        let consoleSpy = sinon.spy(console, 'log')
        validateWithLog(email)
        assert(consoleSpy.calledWith(false))
        consoleSpy.restore()
    })

    it("should return false for given input `m@gmail.com`", () => {
        const email = "m@gmail.com"
        let consoleSpy = sinon.spy(console, 'log')
        validateWithLog(email)
        assert(consoleSpy.calledWith(false))
        consoleSpy.restore()
    })

    it("should return false for given input `example@email.com`", () => {
        const email = "example@email.com"
        let consoleSpy = sinon.spy(console, 'log')
        validateWithLog(email)
        assert(consoleSpy.calledWith(false))
        consoleSpy.restore()
    })
})


describe("validate email async function - tests", () => {
    it("should return true for given input `example@outlook.com`", () => {
        const email = "example@outlook.com"
        validateAsync(email).then((res) => {
            assert.equal(true, res)
            done()
        }).catch((err) => {
            done(err)
        })
    })

    it('should finished before 1s', async function() {
        // should set the timeout of this test to 1000 ms; instead will fail
        this.timeout(1000)
        const email = "example@mail.com"
        const result = await validateAsync(email)
        assert.equal(true, result)
    });


    it("should return true for given input `example@gmail.com`", async () => {
        const email = "example@gmail.com"
        const result = await validateAsync(email)
        assert.equal(true, result)
    })

    it("should return false for given input `@gmail.com`", async () => {
        const email = "@gmail.com"
        assert.rejects(
            validateAsync(email),
            {message: "Invalid emaill"}
        )
    })

    it("should return false for given input `mgail.com`", async () => {
        const email = "mgmail.com"
        assert.rejects(
            validateAsync(email),
            {message: "Invalid emaill"}
        )
    })

    it("should return false for given input `com`", async () => {
        const email = "com"
        assert.rejects(
            validateAsync(email),
            {message: "Invalid emaill"}
        )
    })

    it("should return false for given input `example@email.com`", async () => {
        const email = "example@email.com"
        assert.rejects(
            validateAsync(email),
            {message: "Invalid emaill"}
        )
    })
})

