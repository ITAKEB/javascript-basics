'use strict';

/**
 *
 * @param str: {String}
 * @returns {Boolean}
 */
const isValidJSON = (str) => {
    // Your implementation here
    try {
        JSON.parse(str)
    } catch (e) {
        return false
    }
    return true
};

/**
 *
 * @param params: {Object}
 * @returns {String}
 */
const greeting = ({name, surname, age}) => {
    // Your implementation here
    return `Hello, my name is ${name} ${surname} and I am ${age} years old.`
};

/**
 *
 * @param params: {Array}
 * @returns {Array}
 */
function unique(arr) {
    let aux = arr.filter((item, 
        index) => arr.indexOf(item) === index);
    // Your implementation here
    return aux
}

/**
 * 
 * @param arr: {Array}
 * @return {Iterator}
 */
function* generator(arr) {
    // Your implementation here
        yield* arr
}

module.exports = {
    isValidJSON,
    greeting,
    unique,
    generator
};
