/**
 *
 * @param arr: {Array}
 * @param n: {Number}
 * @returns {Number}
 */
module.exports.nThNoRepeatedValue = function nThNoRepeatedValue(arr, n) {
    // Your implementation here
    arr = arr.map((e, i) => { return { index: i, value: e } })
    let aux = []

    arr.sort((a, b) => {
        if (a.value > b.value) {
            return 1
        } else if (a.value < b.value) {
            return -1
        } else {
            return 0
        }
    })

    for (let i = 0; i < arr.length - 1; i++) {
        if (arr[i].value != arr[i + 1].value) {
            if (!aux.includes(arr[i].value)) {
                aux.push(arr[i])
            }
        }else{
            arr.shift(i,i+1)
        }
    }

    if (arr[arr.length-1].value != arr[arr.length - 2].value) {
        aux.push(arr[arr.length-1])
    }

    aux.sort((a, b) => {
        if (a.index > b.index) {
            return 1
        } else if (a.index < b.index) {
            return -1
        } else {
            return 0
        }
    })

    return aux[n-1].value
};

/**
 *
 * @param arr: {Array}
 * @returns {Array}
 */
module.exports.primeValues = function primeValues(arr) {
    // Your implementation here
    const primes = arr.map((number) => {
        if (number == 1) {
            return null
        }
        let isPrime = true
        for (let i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                isPrime = false;
            }
         }
         return isPrime
    })

    return primes
};
