
async function getRandomUsers(quantity, nationalities) {
    let url = `https://randomuser.me/api/?results=1&nat=&inc=name,email,nat&noinfo`

    if (quantity && nationalities) {
        url = `https://randomuser.me/api/?results=${quantity}&nat=${nationalities}&inc=name,email,nat&noinfo`
    }

    const randomUsers = await fetch(url)
        .then((response) => (response.json()))
        .then((results) => (results))


    return randomUsers.results
}

async function createPost(data) {
    let url = `https://jsonplaceholder.typicode.com/posts`

    // const headers = new Headers()
    // headers.append('Content-Type', 'application/json;charset=utf-8')
    // headers.set('Content-Type', 'application/json;charset=utf-8')

    const postResponse = await fetch(url, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json;charset=UTF-8"
        },
        body: JSON.stringify(data)
    })

    const codeResponse = postResponse.status

    if ([200, 201].includes(codeResponse)) {
        const data = await postResponse.json()

        return data
    } else {
        const error = await postResponse.text()

        return error
    }
}