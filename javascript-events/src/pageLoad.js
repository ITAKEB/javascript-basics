export function pageLoad() {
    // Your implementation here
    window.onbeforeunload = () => false

    window.onload = function() {
        let sectionElement = document.createElement('section')
        sectionElement.classList.add('app-section', 'app-section--image-join-us')
    
        let h2Element = document.createElement('h2')
        h2Element.classList.add('app-title')
        h2Element.innerText = 'Join Our Program'
        sectionElement.appendChild(h2Element)
    
        let h3Element = document.createElement('h3')
        h3Element.classList.add('app-subtitle')
        h3Element.innerHTML = 'Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua'
        sectionElement.appendChild(h3Element)
    
        let formElement = document.createElement('form')
        formElement.classList.add('app-form')
        
        let inputElement = document.createElement('input')
        inputElement.placeholder = 'email'
        inputElement.id = 'emailInput'
        inputElement.name = 'emailInput'
        inputElement.type ='email'
        inputElement.classList.add('app-form__input', 'app-form__input--email')
        
        let buttonElement = document.createElement('button')
        buttonElement.type ='submit'
        buttonElement.id = "subscribeButton"
        buttonElement.name = "subscribeButton"
        buttonElement.classList.add('app-form__button', 'app-form__button--subscribe')
        buttonElement.innerText = 'SUBSCRIBE'
    
        formElement.appendChild(inputElement)
        formElement.appendChild(buttonElement)
    
        sectionElement.appendChild(formElement)
    
        formElement.addEventListener('submit', (evt) => {
            evt.preventDefault()
            console.log(inputElement.value)
    
        })
    
        let appContainer = document.getElementById('app-container')
        let appFooter = document.getElementById('app-footer')
        appContainer.insertBefore(sectionElement, appFooter)
    }
}
