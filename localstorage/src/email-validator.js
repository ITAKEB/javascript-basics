import { VALID_EMAIL_ENDINGS } from "./constants/VALID_EMAIL_ENDINGS.js"


export function validate(email) {
    const reg = /^[a-zA-Z]{3,}\@[a-zA-Z]+\.[a-zA-Z]+$/
    if (!reg.test(email)) {
        return false
    }
    let endingValidation = false
    const emailEnding = email.split('@')[1]
    VALID_EMAIL_ENDINGS.forEach((valid) => {
        if (emailEnding === (valid)) {
            endingValidation = true
        }
    })

    return endingValidation
}