import { validate } from "./email-validator.js"

export function setLocalStorage (email) {
    if (validate(email)) {
        localStorage.setItem("subscriptionEmail", email)
        return true
    }
    return false
}

export function getLocalStorage () {
    const subscriptionEmail = localStorage.getItem("subscriptionEmail")
    return  subscriptionEmail ? subscriptionEmail : ''
}
