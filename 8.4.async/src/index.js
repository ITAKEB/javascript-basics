/**
 *
 * @param data: {Array}
 * @returns number
 */
module.exports.callback1 = function (data) {
    // Your implementation here
    data = data.split(' ')
    let sum = 0
    data.forEach(element => {
        sum += element.length
    });

    return sum
};

/**
 *
 * @param data: {Array}
 * @returns number
 */
module.exports.callback2 = function (data) {
    // Your implementation here
    data = data.split(' ')
    let mul = 1
    data.forEach(element => {
        mul *= element.length
    });

    return mul
};

/**
 *
 * @param s: {string}
 * @returns number
 */
module.exports.w = function (s, callback) {
    // Your implementation here
    return callback(s);
};

/**
 *
 * @param data: {Array | Object}
 * @returns {Function}
 */
module.exports.mocker = function mocker(data) {
    // Your implementation here

    const users = new Promise((resolve, rejected) => {
        setTimeout(() => { resolve(data) }, 1000)
    });

    return () => users
};

// const getUsers = mocker('ola')
// getUsers()
/**
 *
 * @param arg...: {Promise}
 * @returns {Function}
 */
module.exports.summarize1 = function summarize1(...promises) {
    // Your implementation here
    return Promise.all(promises).then((values) => {
        let sum = 0
        values.forEach((value) => sum += value)
        return sum
    })
};

/**
 *
 * @param arg...: {Promise}
 * @returns {Function}
 */
module.exports.summarize2 = async function summarize2(...promises) {
    // Your implementation here
    return await Promise.all(promises).then((values) => {
        let sum = 0
        values.forEach((value) => sum += value)
        return sum
    })
};
