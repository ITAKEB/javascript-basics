export function joinUsStandardSection() {

    window.onbeforeunload = () => false

    window.onload = () => {
        const factory = new SectionCreator()
        const section = factory.create('standard')

        let appContainer = document.getElementById('app-container')
        let appFooter = document.querySelector('footer')

        appContainer.insertBefore(section, appFooter)
    }
}

export function joinUsAdvancedSection() {

    window.onbeforeunload = () => false

    window.onload = () => {
        const factory = new SectionCreator()
        const section = factory.create('advanced')

        let appContainer = document.getElementById('app-container')
        let appFooter = document.querySelector('footer')

        appContainer.insertBefore(section, appFooter)
    }
}

export class SectionCreator {
    create(type) {
        switch (type) {
            case 'standard':
                let standardSection = document.createElement('section')
                standardSection.classList.add('app-section', 'app-section--image-join-us')
                standardSection.id = "join-us-standard"

                let standardTitle = document.createElement('h2')
                standardTitle.classList.add('app-title')
                standardTitle.innerText = 'Join Our Program'
                standardSection.appendChild(standardTitle)

                let standardSubtitle = document.createElement('h3')
                standardSubtitle.classList.add('app-subtitle')
                standardSubtitle.innerHTML = 'Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua'
                standardSection.appendChild(standardSubtitle)

                let standardForm = document.createElement('form')
                standardForm.classList.add('app-form')

                let standardInputForm = document.createElement('input')
                standardInputForm.placeholder = 'email'
                standardInputForm.id = 'emailInput'
                standardInputForm.name = 'emailInput'
                standardInputForm.type = 'email'
                standardInputForm.classList.add('app-form__input', 'app-form__input--email')

                let standardButtonForm = document.createElement('button')
                standardButtonForm.type = 'submit'
                standardButtonForm.id = "subscribeButton"
                standardButtonForm.name = "subscribeButton"
                standardButtonForm.classList.add('app-form__button', 'app-form__button--subscribe')
                standardButtonForm.innerText = 'SUBSCRIBE'

                standardForm.appendChild(standardInputForm)
                standardForm.appendChild(standardButtonForm)

                standardSection.appendChild(standardForm)

                standardForm.addEventListener('submit', (evt) => {
                    evt.preventDefault()
                    console.log(standardInputForm.value)

                })

                return standardSection

            case 'advanced':
                let advancedSection = document.createElement('section')
                advancedSection.classList.add('app-section', 'app-section--image-join-us')
                advancedSection.id = "join-us-advanced"


                let advancedTitle = document.createElement('h2')
                advancedTitle.classList.add('app-title')
                advancedTitle.innerText = 'Join Our Advanced Program'
                advancedSection.appendChild(advancedTitle)

                let advancedSubtitle = document.createElement('h3')
                advancedSubtitle.classList.add('app-subtitle')
                advancedSubtitle.innerHTML = 'Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua'
                advancedSection.appendChild(advancedSubtitle)

                let advancedForm = document.createElement('form')
                advancedForm.classList.add('app-form')

                let advancedInputForm = document.createElement('input')
                advancedInputForm.placeholder = 'email'
                advancedInputForm.type = 'email'
                advancedInputForm.classList.add('app-form__input', 'app-form__input--email')

                let advancedButtonForm = document.createElement('button')
                advancedButtonForm.type = 'submit'
                advancedButtonForm.classList.add('app-form__button', 'app-form__button--subscribe')
                advancedButtonForm.innerText = 'Subscribe to Advanced Program'

                advancedForm.appendChild(advancedInputForm)
                advancedForm.appendChild(advancedButtonForm)

                advancedSection.appendChild(advancedForm)

                advancedForm.addEventListener('submit', (evt) => {
                    evt.preventDefault()
                    console.log(advancedInputForm.value)

                })
                return advancedSection

            default:
                throw new Error("Invalid section type")
        }
    }
}
