class Task {
    // Your implementation here
    constructor(taskName) {
        this.name = taskName
    }
}

class Guest {
    // Your implementation here
    constructor(tasks) {
        this.tasks = tasks
    }

    getTask(index) {
        return this.tasks[index]
    }

    getTasks() { return this.tasks }

    createTask(){
        throw new Error('method \'createTask\' is not defined');
    }

    changeType(){
        throw new Error('method \'changeType\' is not defined');
    }

}

class User {
    // Your implementation here
    constructor(tasks) {
        this.tasks = tasks
    }

    getTask(index) {
        return this.tasks[index]
    }

    createTask(task) {
        this.tasks.push(task)
    }

    getTasks() { return this.tasks }

    changeType(){
        throw new Error('method \'changeType\' is not defined');
    }
}

class Admin {
    // Your implementation here
    constructor(arrUsersAndGuests) {
        this.arrUsersAndGuests = arrUsersAndGuests
    }

    getArray() {
        return this.arrUsersAndGuests
    }

    changeType(index) {
        let info = this.arrUsersAndGuests[index]
        let tasks = info.getTasks()
        if (info instanceof Guest) {
            this.arrUsersAndGuests[index] = new User(tasks)
        }else{
            this.arrUsersAndGuests[index] = new Guest(tasks)
        }
    }

}

module.exports.Task = Task;
module.exports.Guest = Guest;
module.exports.User = User;
module.exports.Admin = Admin;
